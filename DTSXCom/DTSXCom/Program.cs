﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SqlServer.Dts.Design;
using Microsoft.SqlServer.Dts.Runtime;
using Microsoft.SqlServer.Dts.Pipeline.Wrapper;
using Microsoft.SqlServer.Dts.Pipeline;
using Microsoft.SqlServer.VSTAHosting;
using Microsoft.SqlServer.IntegrationServices.VSTA;


/* References 
* // General SSIS references 
* C:\Windows\Microsoft.NET\assembly\GAC_MSIL\Microsoft.SqlServer.Dts.Design\v4.0_11.0.0.0__89845dcd8080cc91\Microsoft.SqlServer.Dts.Design.dll
* C:\Program Files (x86)\Microsoft SQL Server\110\SDK\Assemblies\Microsoft.SqlServer.DTSPipelineWrap.dll 
* C:\Program Files (x86)\Microsoft SQL Server\110\SDK\Assemblies\Microsoft.SQLServer.DTSRuntimeWrap.dll 
* C:\Windows\Microsoft.NET\assembly\GAC_MSIL\Microsoft.SqlServer.PipelineHost\v4.0_11.0.0.0__89845dcd8080cc91\Microsoft.SQLServer.PipelineHost.dll
* C:\Windows\Microsoft.NET\assembly\GAC_MSIL\Microsoft.SqlServer.ScriptTask\v4.0_11.0.0.0__89845dcd8080cc91\Microsoft.SqlServer.ScriptTask.dll
* C:\Program Files (x86)\Microsoft SQL Server\110\SDK\Assemblies\Microsoft.SQLServer.ManagedDTS.dll

* // Script related references 
* C:\Program Files (x86)\Microsoft SQL Server\110\DTS\PipelineComponents\Microsoft.SqlServer.TxScript.dll 
* or C:\Windows\Microsoft.NET\assembly\GAC_MSIL\Microsoft.SqlServer.TxScript\v4.0_11.0.0.0__89845dcd8080cc91\Microsoft.SqlServer.TxScript.dll
* C:\Windows\Microsoft.NET\assembly\GAC_MSIL\Microsoft.SqlServer.VSTAScriptingLib\v4.0_11.0.0.0__89845dcd8080cc91\Microsoft.SqlServer.VSTAScriptingLib.dll
* C:\Windows\Microsoft.NET\assembly\GAC_MSIL\Microsoft.SqlServer.IntegrationServices.VSTA\v4.0_11.0.0.0__89845dcd8080cc91\Microsoft.SqlServer.IntegrationServices.VSTA.dll
*/


namespace RunFromClientAppCS
{
    class Program
    {
        static void Main(string[] args)
        {

            if (args.Length == 0)
            {
                Console.Write("!No package pass in as parameters!\nUsage:\n{0} file1 [file2 file3 ...]\nAfter recomplie, file will get updated with new version.\nPress any key...", Environment.GetCommandLineArgs()[0]);
                Console.ReadKey();
                return;
            }
            else if (args.Length >= 1)
                foreach (string arg in args)
                {
                    try
                    {
                        string pkgLocation;
                        Package pkg;
                        Application app;

                        pkgLocation = arg;
                        app = new Application();
                        if (!System.IO.File.Exists(pkgLocation))
                        {
                            Console.WriteLine("!Path [{0}] is not exits! Skip !", pkgLocation); continue;
                        }
                        pkg = app.LoadPackage(pkgLocation, null);

                        // Obtain the collection.
                        Executables pgkExecs = pkg.Executables;
                        if (pgkExecs.Contains(0))
                        {
                            Console.WriteLine("Contains returned true");
                        }
                        else
                        {
                            Console.WriteLine("Contains returned false");
                        }
                        List<TaskHost> tasks = new List<TaskHost>();
                        foreach (Executable container in pgkExecs)
                        {
                            Digdeep(container);
                        }

                        app.SaveToXml(pkgLocation, pkg, null);
                        Console.WriteLine("!Finish Recompile!\n");
                    }
                    catch (Exception e)
                    {

                        Console.WriteLine(e);

                    }
                }

            // Run the Package to make sure it works. 
            //DTSExecResult pkgResults = pkg.Execute();

            //Console.WriteLine("Package Execution Result = " + pkgResults.ToString());
            Console.WriteLine("Press any key...\n");
            Console.ReadKey();
        }
        private static void Digdeep(Executable container)
        {
            if (container is TaskHost)
            {
                TaskHost taskHost = (TaskHost)container;
                Console.WriteLine("    " + taskHost.Name);

                //Test if the task is a data flow 
                if (taskHost.InnerObject is Microsoft.SqlServer.Dts.Pipeline.Wrapper.MainPipe)
                {

                    //Cast the Executable as a data flow 
                    MainPipe pipe = (MainPipe)taskHost.InnerObject;

                    // Loop over each object in the data-flow 
                    foreach (IDTSComponentMetaData100 comp in pipe.ComponentMetaDataCollection)
                    {

                        Console.WriteLine("       " + comp.Name + "  " + comp.ToString());

                        // Find the Script Components 
                        if (!(comp.Instantiate() is CManagedComponentWrapper))//Microsoft.SqlServer.Dts.Pipeline.ManagedComponentHost)
                            continue;
                        //Recompile the Script Component 
                        CManagedComponentWrapper compWrap = comp.Instantiate();
                        if (!(compWrap is IDTSManagedComponent100) || !(((IDTSManagedComponent100)compWrap).InnerObject is ScriptComponentHost))
                            continue;
                        ScriptComponentHost scriptComp = (compWrap as IDTSManagedComponent100).InnerObject as ScriptComponentHost;
                        Console.WriteLine("!!!Found Script component: " + comp.Name);
                        if (!scriptComp.LoadScriptFromComponent())
                        {
                            throw new Exception("--Failed to load script information from the component");
                        }

                        if (scriptComp.CurrentScriptingEngine.VstaHelper == null)
                        {
                            throw new Exception("--Vsta 3.0 is not installed properly");
                        }

                        if (!scriptComp.CurrentScriptingEngine.LoadProjectFromStorage())
                        {
                            throw new Exception("--Failed to load project files from storage object");
                        }

                        if (!scriptComp.SaveScriptProject())
                        {
                            throw new Exception("--Failed to save project");
                        }
                        Console.WriteLine("+++Recompiled Success: " + comp.Name);
                        scriptComp.CurrentScriptingEngine.DisposeVstaHelper();
                    }
                }
            }
            else if (container is DtsContainer)
            {
                dynamic exe = container;
                Console.WriteLine(exe.Name.ToString());
                dynamic exe1 = container;
                foreach (Executable child in exe1.Executables)
                    Digdeep(child);
            }


        }
    }
}