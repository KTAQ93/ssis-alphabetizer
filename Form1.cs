using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Text.RegularExpressions;
using System.Management;
using System.Diagnostics;
using System.Text;
//using System.Threading;



namespace Alphabetizer
{
    /// <summary>
    /// Summary description for the XML Alphabetizer.
    /// </summary>
    public class XMLAlphabetizer : System.Windows.Forms.Form
    {
        #region Private Members
        private System.Windows.Forms.TextBox txtSource;
        private System.Windows.Forms.Label lblSource;
        private System.Windows.Forms.Button btnGenerate;
        private System.Windows.Forms.Button btnReset;
        private OpenFileDialog openFileDialog;
        private Button btnBrowse;
        private Label lblDepth;
        private NumericUpDown numericUpDown;
        private Label lblSort;
        private TextBox txtAttribute;
        private GroupBox grpOrder;
        private ComboBox comboBox1;


        private static int gui = 0;
        private static bool dtsx = false;
        private static bool AttributeLine = true;
        private static bool LineBreak = true;
        private static XNamespace ns = "www.microsoft.com/SqlServer/Dts";
        private static TextWriter streamw;
        private RichTextBox txtResult; //for element and attribute using namespace instead of normal
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        #endregion

        #region Constructor

        public XMLAlphabetizer()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        #endregion

        #region Dispose
        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }
        #endregion

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(XMLAlphabetizer));
            this.txtSource = new System.Windows.Forms.TextBox();
            this.lblSource = new System.Windows.Forms.Label();
            this.btnGenerate = new System.Windows.Forms.Button();
            this.btnReset = new System.Windows.Forms.Button();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.btnBrowse = new System.Windows.Forms.Button();
            this.lblDepth = new System.Windows.Forms.Label();
            this.numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.lblSort = new System.Windows.Forms.Label();
            this.txtAttribute = new System.Windows.Forms.TextBox();
            this.grpOrder = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.txtResult = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).BeginInit();
            this.grpOrder.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtSource
            // 
            this.txtSource.Location = new System.Drawing.Point(79, 14);
            this.txtSource.Name = "txtSource";
            this.txtSource.Size = new System.Drawing.Size(304, 20);
            this.txtSource.TabIndex = 1;
            // 
            // lblSource
            // 
            this.lblSource.Location = new System.Drawing.Point(12, 18);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(84, 20);
            this.lblSource.TabIndex = 0;
            this.lblSource.Text = "Source XML";
            // 
            // btnGenerate
            // 
            this.btnGenerate.Location = new System.Drawing.Point(389, 47);
            this.btnGenerate.Name = "btnGenerate";
            this.btnGenerate.Size = new System.Drawing.Size(63, 20);
            this.btnGenerate.TabIndex = 8;
            this.btnGenerate.Text = "Generate";
            this.btnGenerate.Click += new System.EventHandler(this.btnGenerate_Click);
            // 
            // btnReset
            // 
            this.btnReset.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnReset.Location = new System.Drawing.Point(389, 77);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(63, 20);
            this.btnReset.TabIndex = 9;
            this.btnReset.Text = "Reset";
            this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
            // 
            // openFileDialog
            // 
            this.openFileDialog.FileName = "openFileDialog";
            this.openFileDialog.Filter = "XML files (*.xml)|*.xml|Text files (*.txt)|*.txt|All files (*.*)|*.*";
            this.openFileDialog.InitialDirectory = ".";
            // 
            // btnBrowse
            // 
            this.btnBrowse.Location = new System.Drawing.Point(389, 13);
            this.btnBrowse.Name = "btnBrowse";
            this.btnBrowse.Size = new System.Drawing.Size(66, 20);
            this.btnBrowse.TabIndex = 2;
            this.btnBrowse.Text = "Browse...";
            this.btnBrowse.UseVisualStyleBackColor = true;
            this.btnBrowse.Click += new System.EventHandler(this.btnBrowse_Click);
            // 
            // lblDepth
            // 
            this.lblDepth.AutoSize = true;
            this.lblDepth.Location = new System.Drawing.Point(12, 47);
            this.lblDepth.Name = "lblDepth";
            this.lblDepth.Size = new System.Drawing.Size(181, 13);
            this.lblDepth.TabIndex = 3;
            this.lblDepth.Text = "Minimum Tree Depth to Apply Sort At";
            // 
            // numericUpDown
            // 
            this.numericUpDown.Location = new System.Drawing.Point(199, 45);
            this.numericUpDown.Name = "numericUpDown";
            this.numericUpDown.Size = new System.Drawing.Size(59, 20);
            this.numericUpDown.TabIndex = 4;
            // 
            // lblSort
            // 
            this.lblSort.AutoSize = true;
            this.lblSort.Location = new System.Drawing.Point(12, 77);
            this.lblSort.Name = "lblSort";
            this.lblSort.Size = new System.Drawing.Size(114, 13);
            this.lblSort.TabIndex = 5;
            this.lblSort.Text = "Sort By Attribute Name";
            // 
            // txtAttribute
            // 
            this.txtAttribute.Location = new System.Drawing.Point(132, 74);
            this.txtAttribute.Name = "txtAttribute";
            this.txtAttribute.Size = new System.Drawing.Size(126, 20);
            this.txtAttribute.TabIndex = 6;
            // 
            // grpOrder
            // 
            this.grpOrder.Controls.Add(this.comboBox1);
            this.grpOrder.Location = new System.Drawing.Point(275, 40);
            this.grpOrder.Name = "grpOrder";
            this.grpOrder.Size = new System.Drawing.Size(108, 54);
            this.grpOrder.TabIndex = 7;
            this.grpOrder.TabStop = false;
            this.grpOrder.Text = "Sort Attributes";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "None",
            "Ascending",
            "Descending"});
            this.comboBox1.Location = new System.Drawing.Point(6, 19);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(96, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.Text = "None";
            // 
            // txtResult
            // 
            this.txtResult.AutoWordSelection = true;
            this.txtResult.ImeMode = System.Windows.Forms.ImeMode.Disable;
            this.txtResult.Location = new System.Drawing.Point(15, 100);
            this.txtResult.Name = "txtResult";
            this.txtResult.ReadOnly = true;
            this.txtResult.Size = new System.Drawing.Size(449, 276);
            this.txtResult.TabIndex = 11;
            this.txtResult.Text = "";
            // 
            // XMLAlphabetizer
            // 
            this.AcceptButton = this.btnGenerate;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.CancelButton = this.btnReset;
            this.ClientSize = new System.Drawing.Size(476, 391);
            this.Controls.Add(this.txtResult);
            this.Controls.Add(this.grpOrder);
            this.Controls.Add(this.txtAttribute);
            this.Controls.Add(this.lblSort);
            this.Controls.Add(this.numericUpDown);
            this.Controls.Add(this.lblDepth);
            this.Controls.Add(this.txtSource);
            this.Controls.Add(this.btnBrowse);
            this.Controls.Add(this.btnReset);
            this.Controls.Add(this.btnGenerate);
            this.Controls.Add(this.lblSource);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "XMLAlphabetizer";
            this.Text = "XML Alphabetizer";
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).EndInit();
            this.grpOrder.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static int Main(string[] args)
        {
            //get parentid and parent object, name
            var myId = Process.GetCurrentProcess().Id;
            var search = new ManagementObjectSearcher("root\\CIMV2", string.Format("SELECT ParentProcessId FROM Win32_Process WHERE ProcessId = {0}", myId));
            var results = search.Get().GetEnumerator();
            results.MoveNext();
            var parentId = (uint)results.Current["ParentProcessId"];
            var parent = Process.GetProcessById((int)parentId);
            string text = "";
            if (args.Length > 0)
            {
                string first = "", third = "", fifth = "";
                int second = 0, fourth = 0;
                foreach (string arg in args)
                {
                    if (arg.Length >= 5 && arg.Substring(0, 3).ToUpper() == "/IN") //(IN) file - Required
                        first = Regex.Replace(Regex.Replace(arg.Substring(4), @"%TMP%|%TEMP%|/tmp/", Path.GetTempPath()), "\"|^[ ]+|[ ]+$", "", RegexOptions.Multiline);
                    if (arg.Length >= 5 && arg.Substring(4) != "Default")
                    {
                        var temp = arg.Substring(4);
                        switch (arg.Substring(0, 3).ToUpper())
                        {
                            case "/LV":  // (LV)evel minimum level to apply sort - Default: 0
                                int.TryParse(temp, out second);
                                if (second < 0) second = 0;
                                break;
                            case "/NS":  // (N)ame Attribute (S)ort - Default: Null
                                third = temp;
                                break;
                            case "/SA":  // (S)ort (A)ttribute - 0: none (default), 1|A: Ascending, 2|D:Descending 
                                if (!int.TryParse(temp, out fourth))
                                {
                                    if (temp.ToUpper() == "A") fourth = 1;
                                    if (temp.ToUpper() == "D") fourth = 2;
                                };
                                break;
                            case "/OU":  // (OU)t file - Default: IN file
                                fifth = temp;
                                break;
                            case "/GU":  // (GU)I enable
                                if (temp != "0" && int.TryParse(temp, out gui) && gui < 0)
                                    gui = 0;
                                else gui = 1;
                                break;
                            case "/IN":
                                break;
                            case "/BR":
                                if (temp == "0")
                                    AttributeLine = false;
                                break;
                            case "/AL":
                                if (temp == "0")
                                    AttributeLine = false;
                                break;
                            default:
                                text = text + arg;
                                // do other stuff...
                                break;
                        }
                    }
                }
                if ((!string.IsNullOrWhiteSpace(first) || (!string.IsNullOrWhiteSpace(first = text) && (fourth = 1) == 1)) && gui == 0) //check INput parameter as required
                {
                    try
                    {
                        if (fifth == "")    //set output to same file as input if no value
                            fifth = first;
                        if (fifth == "-")     //set output to standard outstream if using -
                            using (streamw = new StreamWriter(Console.OpenStandardOutput(8192)))
                            {
                                if ((third = RunSort(first, second, third, fourth)) == null) return 1;
                            }
                        else
                        {   //handle situation when output in same folder as exe
                            if (!string.IsNullOrWhiteSpace(Path.GetDirectoryName(fifth)) && !Directory.Exists(Path.GetDirectoryName(fifth)))
                                Directory.CreateDirectory(Path.GetDirectoryName(fifth));
                            using (streamw = new StringWriter())
                            {
                                if ((third = RunSort(first, second, third, fourth)) == null) return 1;
                                File.WriteAllText(fifth, streamw.ToString());
                            };
                        }
                        return 0;
                    }
                    catch (System.Security.SecurityException)
                    {   //catch security issue
                        MessageBox.Show(string.Format("Error: {0}\\{1} on {2} does not have access to {3} or {4}", Environment.UserDomainName, Environment.UserName, Environment.MachineName, first, fifth), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 4;
                    }
                    catch (Exception exc)
                    {   //catch all other issue
                        MessageBox.Show(first + ' ' + second + ' ' + third + ' ' + fourth + ' ' + fifth + string.Format("\nError: {0}", exc), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return 3;
                    }
                }
            }
            //only open GUI if parent is explorer or VS, else it runs in script so only need to return error code
            if (parent.ProcessName != "explorer" && parent.ProcessName != "devenv" && gui == 0)
            {
                if (parent.ProcessName == "cmd" || parent.ProcessName == "bash") //if call from cmd then return usage help
                {
                    var usage = string.Format(Environment.CommandLine + "\nError! Parameters are not correct.\n\nUsage:\n" +
                    "{0} /IN:File [/OU:File] [/SA:0|1|2] [/NS:Value] [/LV:Value] [/BR:0|1|2] [/AL:0|1]\n\n" +
                    "/IN: Path to (IN)put file.\n" +
                    "/OU: Path to (OU)tput file - (Default): INput file, -:Console.\n" +
                    "/NS: (N)ame of attribute to (S)ort all elements - Default: Null.\n" +
                    "/SA: Direction (S)ort (A)ttributes-1|(A)scend,2|(D)escend,None(Default).\n" +
                    "/LV: Level to start sorting. Root is 0\n" +
                    "/BR: New line type - 0:Auto| 1:Windows| 2:Linux\n" +
                    "/AL: New line for each attribute - 0:No| 1:Yes (Default)\n\n" +
                    "Use 'Default' or keep empty to get default value of flag.\n\n",
                    Environment.GetCommandLineArgs()[0]);

                    if (!AttachConsole(-1))
                        MessageBox.Show(usage, "Error!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                        Console.Write(usage);
                }
                return 2;  //error in the parameters
            }
            Application.Run(new XMLAlphabetizer());
            return 0;

        }

        #region Public Method - RunSort
        /// <summary>
        /// Run the sorting routine for the XML document.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static string RunSort(string sourceStr, int level, string attribute, int sortAttributes)
        {
            if (sourceStr == "-" && gui == 0)
            {
                sourceStr = "";
                using (TextReader a = new StreamReader(Console.OpenStandardInput(8192)))
                using (PeekableTextReader s = new PeekableTextReader(a))
                {
                    if (s.PeekLine().Contains(@"<!--refId"))
                    {
                        streamw.Write(Regex.Replace(s.ReadToEnd(), "[\r\n]+", "\r\n")); return "";
                    }
                    sourceStr = s.ReadToEnd();
                }
            }
            else if (File.Exists(sourceStr))
            {
                using (PeekableTextReader s = new PeekableTextReader(new StreamReader(new FileStream(sourceStr, FileMode.Open))))
                {
                    if (s.PeekLine().Contains(@"<!--refId"))
                    {
                        streamw.Write(Regex.Replace(s.ReadToEnd(), "[\r\n]+", "\r\n")); return "";
                    }

                    using (var reader = XmlReader.Create(s))
                    {
                        if (!reader.Read())
                        {
                            return null;
                        }
                        reader.Close();
                    }
                }
                sourceStr = File.ReadAllText(sourceStr);
            }
            else if (!File.Exists(sourceStr))//INput sourceStr not exist
            {
                MessageBox.Show(string.Format("File Not Found: {0}", sourceStr), "File Not Found Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return null;
            } //MessageBox.Show(sourceStr + "!");
            if (sourceStr.Contains("DTS:Executable xmlns:DTS=\"www.microsoft.com/SqlServer/Dts\""))
                dtsx = true;
            sourceStr = Regex.Replace(sourceStr, @"<([A-Z][A-Z0-9]*)(\b[^>]*)></\1>", @"<$1$2 />", RegexOptions.IgnoreCase | RegexOptions.Compiled).Replace(@"�", @"(C)");
            if (dtsx) streamw.WriteLine("<!--refId");//add xml comment section at top for easy compare
            XDocument Sorted = Sort(XDocument.Parse(sourceStr), level, sourceStr.Contains("SsisUnit.xsd") ? "name" : attribute, (dtsx) ? 1 : sortAttributes);
            if (dtsx) streamw.WriteLine("-->");
            using (XmlWriter xmlWriter = XmlWriter.Create(streamw, new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true, NewLineOnAttributes = AttributeLine, NewLineChars = (LineBreak) ? "\r\n" : "\n" }))
            {
                Sorted.WriteTo(xmlWriter);
            }
            return ""; //replace special � character;
            //value of elements on newline
        }
        #endregion

        #region !!!TODO list!!!
        //TODO: Add code to auto complie script component http://blogs.msdn.com/b/jason_howell/archive/2013/03/05/script-component-recompile-in-sql-server-2012-integration-services-ssis-to-refresh-metadata.aspx
        //TODO: Fix input by insert from stdin
        //TODO: split specail rules into special function instead of run at this high level general sort
        //TODO: sort by attributes' value needs to handle Case-insensitive
        //TODO: sort by attributes' value needs to handle list of attributes
        #endregion


        #region Private Method - Sort

        /// <summary>
        /// Sort an XML Element based on a minimum level to perform the sort from and either based on the value
        /// of an attribute of an XML Element or by the name of the XML Element.
        /// </summary>
        /// <param name="file">File to load and sort</param>
        /// <param name="level">Minimum level to apply the sort from.  0 for root level.</param>
        /// <param name="attribute">Name of the attribute to sort by.  "" for no sort</param>
        /// <param name="sortAttributes">Sort attributes none, ascending or descending for all sorted XML nodes</param>
        /// <returns>Sorted XElement based on the criteria passed in.</returns>

        private static XDocument Sort(XDocument file, int level, string attribute, int sortAttributes)
        {
            return new XDocument(Sort(file.Root, level, attribute, sortAttributes));
        }


        /// <summary>
        /// Sort an XML Element based on a minimum level to perform the sort from and either based on the value
        /// of an attribute of an XML Element or by the name of the XML Element.
        /// </summary>
        /// <param name="element">Element to sort</param>
        /// <param name="level">Minimum level to apply the sort from.  0 for root level.</param>
        /// <param name="attribute">Name of the attribute to sort by.  "" for no sort</param>
        /// <param name="sortAttributes">Sort attributes none, ascending or descending for all sorted XML nodes</param>
        /// <returns>Sorted XElement based on the criteria passed in.</returns>

        private static XElement Sort(XElement element, int level, string attribute, int sortAttributes)
        {
            XElement newElement;
            if (element.Name == "arrayElements" && dtsx && element.Ancestors().Count() > level && element.HasAttributes && string.IsNullOrEmpty(attribute) && Int32.Parse(element.Attribute("arrayElementCount").Value) != 0 && Int32.Parse(element.Attribute("arrayElementCount").Value) % 3 == 0)
            {
                //create "replace" element
                newElement = new XElement("arrayElements", element.Elements()
                    .Select((x, i) => new { Index = i, Value = x }) //put all child elements into index list
                    .GroupBy(x => x.Index / 3)                      //split into groups 3:file,format,content
                    .Select(x => x.Select(v => v.Value).ToList())   //group 3 elements into 1 big element
                    .ToList().OrderBy(f => f.ElementAt(0).Value));  //group all groups to list then order by first element's value of small groups
            }
            else
            {
                bool common = false;
                if (dtsx && element.Ancestors().Count() > level && element.HasAttributes && string.IsNullOrEmpty(attribute) && element.Attribute("refId") != null || element.Attribute(ns + "refId") != null)
                    streamw.Write("refId=\"{0}\"\r\n",
                        element.Attribute("refId") != null ? element.Attribute("refId").Value :
                        element.Attribute(ns + "refId") != null ? element.Attribute(ns + "refId").Value : "");
                newElement = new XElement(element.Name,
                    from child in element.Elements()
                    orderby
                        (child.Ancestors().Count() > level && child.Name.ToString() == ns + "Property" &&
                            child.Attribute(ns + "Name") != null &&
                            child.Attribute(ns + "Name").Value == "PackageFormatVersion") ? 1 : 2,
                        (child.Ancestors().Count() > level) ? child.Name.ToString() : "",    //sort by element name first
                        (child.Ancestors().Count() > level) ?
                            (
                                (child.HasAttributes && !string.IsNullOrEmpty(attribute) && child.Attribute(attribute) != null)
                                ? child.Attribute(attribute).Value.ToString()           //sort by value of special attribute second
                                : ""//child.Value.ToString()
                            ) : "",

                        (((common = (dtsx && child.Ancestors().Count() > level && child.HasAttributes && string.IsNullOrEmpty(attribute))) && child.Attribute("refId") != null && (element.Name == "inputs"))
                            ? child.Attribute("refId").Value : "") descending,
                        (common && child.Attribute("refId") != null && (element.Name == "outputs"))
                            ? ((child.Attribute("refId").Value.Contains("No Match Output"))
                                ? 2 : (child.Attribute("refId").Value.Contains("Error Output")) ? 3 : 1) : 0,
                        (common && (element.Name == "paths") && child.Attribute("startId") != null)
                            ? child.Attribute("startId").Value : "",
                        (common && child.Attribute(ns + "refId") != null)
                            ? child.Attribute(ns + "refId").Value : "",
                        (common && child.Attribute(ns + "ObjectName") != null)
                            ? child.Attribute(ns + "ObjectName").Value : "",
                        (common && child.Attribute("refId") != null)
                            ? child.Attribute("refId").Value : "",
                        (common && child.Attribute("name") != null)
                            ? child.Attribute("name").Value : "",
                        (common && child.Attribute("Name") != null)
                            ? child.Attribute("Name").Value : "",
                        (common && child.Attribute("Id") != null)
                            ? child.Attribute("Id").Value : "",
                        (child.Ancestors().Count() > level && child.HasAttributes) ?
                            child.Attributes().OrderBy(x => x.Name.ToString()).ThenBy(x => x.Value).FirstOrDefault().Name.ToString() : "",
                        (child.Ancestors().Count() > level && child.HasAttributes) ?
                            child.Attributes().OrderBy(x => x.Name.ToString()).ThenBy(x => x.Value).FirstOrDefault().Value.ToString() : "",
                        (child.Ancestors().Count() > level && child.HasElements && !string.IsNullOrWhiteSpace(child.Value))
                            ? child.Value : ""

                    //End of the orderby clause
                    select Sort(child, level, attribute, sortAttributes));
            }
            if (element.HasAttributes)
            {
                switch (sortAttributes)
                {
                    case 0: //None
                        foreach (XAttribute attrib in element.Attributes())
                        {
                            newElement.SetAttributeValue(attrib.Name, attrib.Value);
                        }
                        break;
                    case 1: //Ascending
                        foreach (XAttribute attrib in element.Attributes().OrderBy(a => a.Name.ToString()))
                        {
                            newElement.SetAttributeValue(attrib.Name, attrib.Value);
                        }
                        break;
                    case 2: //Descending
                        foreach (XAttribute attrib in element.Attributes().OrderByDescending(a => a.Name.ToString()))
                        {
                            newElement.SetAttributeValue(attrib.Name, attrib.Value);
                        }
                        break;
                    default:
                        break;
                }
            }

            // Add original value of elements/nodes
            if (!element.HasElements)
            {
                //even element has no child element but it still have child node contain CDATA

                if (!element.IsEmpty)
                {
                    foreach (var child in element.Nodes())
                    {
                        //get CDATA
                        if (child.NodeType.ToString() == @"CDATA")
                            newElement.Add(new XCData(element.Value));
                        //get TEXT
                        else newElement.Add(element.Value);
                    }
                }

            }
            return newElement;
        }
        #endregion

        #region Event Handler - btnGenerate_Click
        /// <summary>
        /// Click event for the btnGenerate button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnGenerate_Click(object sender, System.EventArgs e)
        {
            txtResult.Text = @"Working!";
            if (string.IsNullOrEmpty(txtSource.Text.Trim()))
            {
                MessageBox.Show("Enter a filename!", "File Name Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return;
            }
            try
            {
                StringBuilder sb = new StringBuilder();
                using (streamw = new StringWriter(sb))
                {
                    txtResult.Text = (RunSort(txtSource.Text, (int)numericUpDown.Value, txtAttribute.Text, comboBox1.SelectedIndex) == null) ? "!Error in input file!" : sb.ToString();
                }
            }

            catch (System.Security.SecurityException)
            {
                MessageBox.Show(string.Format("Error: {0}\\{1} on {2} does not have access to {3}", Environment.UserDomainName, Environment.UserName, Environment.MachineName, txtSource.Text), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (FileNotFoundException filexc)
            {
                MessageBox.Show(string.Format("File Not Found: {0}", filexc), "File Not Found Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            catch (Exception exc)
            {
                MessageBox.Show(string.Format("Error: {0}", exc), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        #endregion

        #region Event Handler - btnReset_Click
        /// <summary>
        /// Click handler for the btnReset button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReset_Click(object sender, System.EventArgs e)
        {
            txtSource.Text = "";
            txtResult.Text = "";
            numericUpDown.Value = 0;
            txtAttribute.Text = "";
            comboBox1.SelectedIndex = 0;
            txtSource.Focus();
        }
        #endregion

        #region Event Handler - btnBrowse_Click
        /// <summary>
        /// Click handler for the btnBrowse button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnBrowse_Click(object sender, EventArgs e)
        {
            if (openFileDialog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                txtSource.Text = openFileDialog.FileName;
            }
        }
        #endregion


        [System.Runtime.InteropServices.DllImport("kernel32.dll")]
        private static extern bool AttachConsole(
            int pid);
    }
    public class PeekableTextReader : TextReader
    {
        private TextReader _Underlying;
        private MemoryStream _Buffer;
        private StreamReader _BufferReader;
        private StreamWriter _BufferWriter;
        private long _BufferPosition = 0;

        public PeekableTextReader(TextReader underlying)
        {
            _Underlying = underlying;
            _Buffer = new System.IO.MemoryStream();
            _BufferReader = new StreamReader(_Buffer);
            _BufferWriter = new StreamWriter(_Buffer);
        }

        public string PeekLine()
        {
            string line = _Underlying.ReadLine();
            if (line == null) return null;

            _Buffer.Seek(0, SeekOrigin.End);
            _BufferWriter.WriteLine(line);
            _BufferWriter.Flush();
            _Buffer.Seek(_BufferPosition, SeekOrigin.Begin);
            return line;
        }

        public override int Peek()
        {
            var read = _Underlying.Read();
            _Buffer.Seek(0, SeekOrigin.End);
            _BufferWriter.Write(read);
            _BufferWriter.Flush();
            _Buffer.Seek(_BufferPosition, SeekOrigin.Begin);
            return read;
        }

        public override void Close()
        {
            _Underlying.Close();
        }

        public override System.Runtime.Remoting.ObjRef CreateObjRef(Type requestedType)
        {
            return _Underlying.CreateObjRef(requestedType);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                TryDispose(ref _Underlying);
                TryDispose(ref _Buffer);
                TryDispose(ref _BufferReader);
                TryDispose(ref _BufferWriter);
            }
        }

        private void TryDispose<T>(ref T obj) where T : class, IDisposable
        {
            try
            {
                if (obj != null)
                {
                    obj.Dispose();
                }
            }
            catch (Exception) { }
            obj = null;
        }

        public override bool Equals(object obj)
        {
            return _Underlying.Equals(obj);
        }

        public override int GetHashCode()
        {
            return _Underlying.GetHashCode();
        }

        public override object InitializeLifetimeService()
        {
            return _Underlying.InitializeLifetimeService();
        }

        public override string ReadLine()
        {
            if (_Buffer.Length > _BufferPosition)
            {
                var line = ReadFromBuffer(rdr => rdr.ReadLine());
                return line;
            }
            else
            {
                return _Underlying.ReadLine();
            }
        }

        public override int Read()
        {
            if (_Buffer.Length > _BufferPosition)
            {
                return ReadFromBuffer(rdr => rdr.Read());
            }
            else
            {
                return _Underlying.Read();
            }
        }

        private T ReadFromBuffer<T>(Func<StreamReader, T> cmd)
        {
            if (_BufferPosition != _Buffer.Position)
            {
                _Buffer.Seek(_BufferPosition, SeekOrigin.Begin);
            }
            var value = cmd(_BufferReader);
            _BufferPosition = _Buffer.Position;
            return value;
        }

        public override int Read(char[] buffer, int index, int count)
        {
            if (_Buffer.Length > _BufferPosition)
            {
                var read = ReadFromBuffer(rdr => rdr.Read(buffer, index, count));
                if (count > read && _Buffer.Position == _Buffer.Length)
                {
                    read += _Underlying.Read(buffer, index + read, count - read);
                }
                return read;
            }
            else
            {
                return _Underlying.Read(buffer, index, count);
            }
        }

        public override int ReadBlock(char[] buffer, int index, int count)
        {
            if (_Buffer.Length > _BufferPosition)
            {
                var read = ReadFromBuffer(rdr => rdr.ReadBlock(buffer, index, count));
                if (count > read && _Buffer.Position == _Buffer.Length)
                {
                    read += _Underlying.ReadBlock(buffer, index + read, count - read);
                }
                return read;
            }
            else
            {
                return _Underlying.ReadBlock(buffer, index, count);
            }
        }

        public override string ReadToEnd()
        {
            if (_Buffer.Length > _BufferPosition)
            {
                return ReadFromBuffer(rdr => rdr.ReadToEnd()) + _Underlying.ReadToEnd();
            }
            else
            {
                return _Underlying.ReadToEnd();
            }
        }
    }
}
